const Deck = require("./deck");

class Player {
    constructor(name, deck = new Deck([])) {
        this.name = name;
        this.deck = deck;
        this.foldsWon = [];
        this.points = 0;
    }

    receiveCard(card) {
        this.deck.push(card);
    }

    foldWon(cards) {
        cards.forEach(card => this.foldsWon.push(card));
    }

    clear() {
        delete this.foldsWon;
        delete this.deck;
        this.foldsWon = [];
        this.deck = undefined;
    }
}

module.exports = Player;