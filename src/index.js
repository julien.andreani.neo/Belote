// const express = require("express");

// const api = express();
// api.use(express.static(`${__dirname}/public`));

// api.listen(3000, () => console.log("API up and running"));

const Game = require("./game");

const game = new Game();
game.addPlayer("player1");
game.addPlayer("player2");
game.addPlayer("player3");
game.addPlayer("player4");
game.init();