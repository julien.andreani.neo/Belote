const SUITS = ["♣", "♦", "♥", "♠"];
const VALUES = ["7", "8", "9", "10", "J", "Q", "K", "A"];
const CARD_WEIGHT_MAP = {
    "7": 7,
    "8": 8,
    "9": 9,
    "10": 10,
    "J": 11,
    "Q": 12,
    "K": 13,
    "A": 14
};
const CARD_VALUE_MAP = {
    "7": 0,
    "8": 0,
    "9": 0,
    "J": 2,
    "Q": 3,
    "K": 4,
    "10": 10,
    "A": 11
};
const TRUMP_VALUE_MAP = {
    "7": 0,
    "8": 0,
    "Q": 3,
    "K": 4,
    "10": 10,
    "A": 11,
    "9": 14,
    "J": 20
};

class Deck {
    constructor(cards = freshDeck()) {
        this.cards = cards;
    }

    get length() {
        return this.cards.length;
    }

    pop() {
        return this.cards.shift();
    }

    push(card) {
        this.cards.push(card);
    }

    shuffle() {
        for (let i = this.length - 1; i > 0; i--) {
            const newId = Math.floor(Math.random() * (i + 1));
            const old = this.cards[newId];
            this.cards[newId] = this.cards[i];
            this.cards[i] = old;
        }
    }

    display() {
        let out = "";
        this.cards.forEach(card => out += `${card.value}${card.suit} `);
        console.log(out);
    }
}

class Card {
    constructor(suit, value) {
        this.suit = suit;
        this.value = value;
    }

    get color() {
        return this.suit === "♣" || this.suit === "♠" ? "black" : "red";
    }
}

function freshDeck() {
    return SUITS.flatMap(suit => {
        return VALUES.map(value => {
            return new Card(suit, value);
        });
    });
}

module.exports = Deck;
module.exports.trumpValueMap = TRUMP_VALUE_MAP;
module.exports.cardValueMap = CARD_VALUE_MAP;