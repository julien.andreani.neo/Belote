const Deck = require("./deck");
const Player = require("./player");

class Game {
    constructor() {
        this.players = [];
        this.deck = new Deck();
    }

    addPlayer(name) {
        this.players.push(new Player(name));
    }

    init() {
        if (this.players.length !== 4)
            return;
        this.deck.shuffle();
        this.players[0].receiveCard(this.deck.pop());
        this.players[0].receiveCard(this.deck.pop());
        this.players[0].receiveCard(this.deck.pop());
        this.players[1].receiveCard(this.deck.pop());
        this.players[1].receiveCard(this.deck.pop());
        this.players[1].receiveCard(this.deck.pop());
        this.players[2].receiveCard(this.deck.pop());
        this.players[2].receiveCard(this.deck.pop());
        this.players[2].receiveCard(this.deck.pop());
        this.players[3].receiveCard(this.deck.pop());
        this.players[3].receiveCard(this.deck.pop());
        this.players[3].receiveCard(this.deck.pop());
        this.players[0].receiveCard(this.deck.pop());
        this.players[0].receiveCard(this.deck.pop());
        this.players[0].receiveCard(this.deck.pop());
        this.players[1].receiveCard(this.deck.pop());
        this.players[1].receiveCard(this.deck.pop());
        this.players[1].receiveCard(this.deck.pop());
        this.players[2].receiveCard(this.deck.pop());
        this.players[2].receiveCard(this.deck.pop());
        this.players[2].receiveCard(this.deck.pop());
        this.players[3].receiveCard(this.deck.pop());
        this.players[3].receiveCard(this.deck.pop());
        this.players[3].receiveCard(this.deck.pop());
        this.players[0].receiveCard(this.deck.pop());
        this.players[0].receiveCard(this.deck.pop());
        this.players[1].receiveCard(this.deck.pop());
        this.players[1].receiveCard(this.deck.pop());
        this.players[2].receiveCard(this.deck.pop());
        this.players[2].receiveCard(this.deck.pop());
        this.players[3].receiveCard(this.deck.pop());
        this.players[3].receiveCard(this.deck.pop());
    }
}

module.exports = Game;